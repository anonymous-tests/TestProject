from application.run import app
import json

def testAPI():
    app_response = app.test_client().get('/healthcheck')
    response_data = json.loads(app_response.get_data(as_text = True))
    assert response_data['up'] == "y"

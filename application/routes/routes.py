from flask import jsonify, request, Response, Blueprint
from flask_swagger import swagger
from SQL.Data import Data

routes = Blueprint('routes', __name__,)

@routes.route("/api")
def homeAPI():
    return jsonify(applicationInfo())

@routes.route("/api/data/add/", methods = ["POST"])
def addDataKeyValue():
    key = request.form.get('key', type = str)
    value = request.form.get('value', type = str)
    inserted = Data().insert((key, value,))
    if inserted == 'ENTRY_ALREADY_EXISTS':
        return jsonify(message = inserted), 409
    elif inserted is True:
        return jsonify(message = "Insertion successful."), 201
    else:
        return jsonify(message = "INSERTION_ERROR"), 500

@routes.route("/api/data/<string:key>", methods = ["GET"])
def getDataValue(key):
    data = Data().getValueFromKey(key)
    return jsonify(result = data) if data else jsonify(message = "KEY_ENTRY_NOT_FOUND")

@routes.route("/receive-message", methods = ["GET"])
def receiveMessage():
    return defaultMessage()

@routes.route("/send-message", methods = ["POST"])
def sendMessage():
    if request.is_json:
        data_received = request.get_json()
        if data_received['question'] == "test":
            return defaultMessage()
    return app.register_error_handler(404)

@routes.route("/healthcheck", methods = ["GET"])
def healthCheck():
    return jsonify(up = "y")

def defaultMessage():
    return jsonify(msg = "test Message")

def applicationInfo():
    from flask import current_app as app
    app_info = swagger(app)
    app_info['info']['version'] = "1.1"
    app_info['info']['title'] = "Simple API Test."
    return app_info
import mysql.connector
import json

class Database:

    def __init__(self: object):
        if not hasattr(self, 'connection'):
            with open('database.json', 'r') as config_file:
                self.configuration = json.load(config_file)
            self.connect()

    def connect(self: object):
        self.connection = mysql.connector.connect(
            **
            {
                'host' : self.configuration['HOST'],
                'database' : self.configuration['MYSQL_DATABASE'],
                'user' : self.configuration['MYSQL_USER'],
                'password' : self.configuration['MYSQL_PASSWORD'],
            }
        )
    
    def disconnect(self: object):
        self.connection.close()

    def createCursor(self: object):
        if not self.connection:
            self.connect()
        self.cursor = self.connection.cursor()

    def closeCursor(self: object):
        if hasattr(self, 'cursor'):
            self.cursor.close()
from flask.json import jsonify
from SQL.Database import Database as Parent

class Data(Parent):

    TABLE = "data"
    
    GET_VALUE_FROM_KEY_QUERY = f"SELECT value from {TABLE} where `key` = %s"
    INSERT_NEW_ROW = f"INSERT INTO {TABLE}(`key`, value) VALUES (%s, %s)"

    def getValueFromKey(self: object, key: str):
        result = ''
        try:
            self.__checkCursorSetted()
            self.cursor.execute(self.GET_VALUE_FROM_KEY_QUERY, (key,))
            result = self.cursor.fetchone()[0]
        except Exception as e:
            pass
            # super().Log().error(e)
        finally:
            super().closeCursor()
            return result

    def insert(self: object, values: tuple):
        if self.__checkEntryExist(values[0]) is True:
                return "ENTRY_ALREADY_EXISTS"
        try:
            self.__checkCursorSetted()
            self.cursor.execute(self.INSERT_NEW_ROW, values)
            self.connection.commit()
        except Exception as e:
            pass
            # super().Log().error(e)
        finally:
            super().closeCursor()
            return self.__checkEntryExist(values[0])
            

    def __checkCursorSetted(self: object):
        if not hasattr(super(), 'connection'):
            super().createCursor()

    def __checkEntryExist(self: object, key: str):
        value = self.getValueFromKey(key)
        return True if value else False

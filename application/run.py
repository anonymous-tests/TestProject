from flask import Flask
from routes import routes
import logging

app = Flask(__name__)

logging.basicConfig(
        handlers = [
            logging.FileHandler('logs/general.log', 'w', 'utf-8')
        ]
)

app.register_blueprint(routes.routes)

# Handle all 404:
@app.errorhandler(404)
def notFound(error):
   return routes.Response(status = 418)

if __name__ == "__main__":
    app.run(debug = True, host = '0.0.0.0')
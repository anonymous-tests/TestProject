FROM alpine

RUN apk add python3 py3-pip
RUN pip3 install flask flask-swagger mysql-connector-python

ENV HOME /home
ENV FLASK_ENV development

COPY application /home/application

EXPOSE 5000

STOPSIGNAL SIGTERM

WORKDIR /home/application

ENTRYPOINT [ "/usr/bin/python3" ]
CMD ["run.py"]
